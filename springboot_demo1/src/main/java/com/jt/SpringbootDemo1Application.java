package com.jt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootDemo1Application {

	//开箱即用效果! SpringBoot如何实现的!!!!  程序调用内部结构过程是什么 面试题  只会CURD.
	public static void main(String[] args) {
		//程序启动
		SpringApplication.run(SpringbootDemo1Application.class, args);
	}

}
