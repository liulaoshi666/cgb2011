package com.jt.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RedisController {

    //需求: 从spring容器中动态获取属性值   表达式:springel表达式 简称spel表达式
    @Value("${redis.host}")
    private String host;    // = "10.0.0.4";
    @Value("${redis.port}")
    private Integer port;   // = 6380;

    @RequestMapping("/getNode")
    public String getNode(){

        return "redis节点:"+host+":"+port;
    }
}
