package com.jt.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//动态加载配置文件
@PropertySource(value = "classpath:/properties/redis.properties",encoding = "UTF-8")
public class RedisProController {

    @Value("${redisPro.host}")
    private String host;
    @Value("${redisPro.port}")
    private Integer port;

    @RequestMapping("/getNodePro")
    public String getNode(){

        return "redis节点:"+host+":"+port;
    }
}
