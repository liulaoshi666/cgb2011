package com.jt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data   //动态添加get/set/toString/equals等方法
@AllArgsConstructor //添加构造方法
@NoArgsConstructor  //添加无参构造
@Accessors(chain = true)    //引入链式加载方式  重写set方法
public class User {

    private Integer id;
    private String name;
    private Integer age;
    private String sex;

   /* public User setId(Integer id){
        this.id = id;
        return this;
    }

    public User setName(String name){
        this.name = name;
        return this;
    }*/
}
