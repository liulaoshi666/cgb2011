package com.jt;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//引入mybatis接口包路径
@MapperScan("com.jt.mapper")
public class SpringbootDemo2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootDemo2Application.class, args);
	}

}
