package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

//@Mapper()     //为接口创建代理对象 交给Spring管理
//注意事项!!!!!!  继承父级接口时必须添加泛型
public interface UserMapper extends BaseMapper<User> {

    //查询全部用户数据 注解方式|xml映射文件方式
    List<User> findAll();

}
