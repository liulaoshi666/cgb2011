package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data   //动态添加get/set/toString/equals等方法
@AllArgsConstructor //添加构造方法
@NoArgsConstructor  //添加无参构造
@Accessors(chain = true)    //引入链式加载方式  重写set方法
@TableName("user")          //引入表名
public class User implements Serializable {         //规则: 如果字段名称与属性一致(包含驼峰规则) 则可以省略不写

    @TableId(type = IdType.AUTO)    //标识主键,主键自增
    private Integer id;
    @TableField("name") //省略之后,程序自动配置
    private String name;
    private Integer age;
    private String sex;







   /* public User setId(Integer id){
        this.id = id;
        return this;
    }

    public User setName(String name){
        this.name = name;
        return this;
    }*/
}
