package com.jt;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.awt.image.AreaAveragingScaleFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest  //当程序执行@Test方法时,会先启动springBoot的容器实例化对象,.之后程序调用
class SpringbootDemo2ApplicationTests {

	//动态注入mapper接口的实例对象 代理对象  cgblib  jdk
	@Autowired	//依赖注入 1.按照类型注入   2.按照名称注入
	//@Qualifier("注入属性的名称")
	private UserMapper userMapper;


	@Test
	public void testMybatis() {
		System.out.println("输入代理对象的类型:"+userMapper.getClass()+"JDK代理");
		List<User> userList = userMapper.findAll();
		System.out.println(userList);
	}


	@Test
	public void testMP(){
		//几乎单表不写sql      查询所有的user表数据 不写where条件
		List<User> userList = userMapper.selectList(null);
		System.out.println(userList);
	}


	@Test
	public void insert(){
		User user = new User();
		user.setName("测试Mybatis").setAge(10).setSex("男");
		userMapper.insert(user);
		//准备好了入库的模板
	}

	/**
	 * MP练习1: 查询
	 * 1. 根据Id查询数据 id=5的用
	 * 2. 根据name="唐僧" age=30    Sql: where name=xxx and age=xxx
	 */
	@Test
	public void testSelect01(){

		User user = userMapper.selectById(5);
		System.out.println(user);

		User user2 = new User();
		user2.setName("唐僧").setAge(30);
		//条件构造器 动态拼接where条件 原则: 根据对象中不为null的属性动态拼接where条件.
		QueryWrapper queryWrapper = new QueryWrapper(user2);
		//Sql: select xxx,xxx,xx from user where name="唐僧" and age=30
		User user3 = userMapper.selectOne(queryWrapper);
		System.out.println(user3);
	}

	/**
	 * 需求: 查询age>18岁的 性别为女的用户  sex='女'
	 * 特殊符号:  字段左侧 > gt   < lt   = eq
	 * 		     >= ge  <= le
	 */
	@Test
	public void testSelect02(){
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.gt("age", 18);
		queryWrapper.eq("sex", "女");
		List<User> userList = userMapper.selectList(queryWrapper);
		System.out.println(userList);
	}

	/**
	 * 案例3: 查询name中包含'精'字的用户,并且性别为女
	 * 		  like %精%
	 * 		  like 精%
	 * 		  like %精
	 */
	@Test
	public void testSelect03(){
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.like("name", "精");
		queryWrapper.eq("sex", "女");
		List<User> userList = userMapper.selectList(queryWrapper);
		System.out.println(userList);
	}

	/**
	 * 案例4: 要求age>18岁, 并且name字段不为null的用户. 按照年龄降序排列.
	 */
	@Test
	public void testSelect04(){
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.gt("age", 18);
		queryWrapper.isNotNull("name");
		queryWrapper.orderByDesc("age");

		List<User> userList = userMapper.selectList(queryWrapper);
		System.out.println(userList);
	}

	/**
	 * 案例5:
	 * 		 查询用户信息  有2个参数,但是2个参数可能部分为null 动态查询
	 * 		 根据sex和age查询  sex  和 age 可能为null???
	 * 		 逻辑运算符  age > 18  and sex="女"
	 *
	 */
	@Test
	public void testSelect05(){
		User user = new User();	//模拟用户传参
		user.setAge(18);
		//user.setSex("女");

		QueryWrapper queryWrapper = new QueryWrapper();

		//动态sql写法 condition如何条件为true时,该条件才拼接.
		queryWrapper.gt(user.getAge() !=null && user.getAge()>0, "age", user.getAge());
		queryWrapper.eq(!"".equals(user.getSex()) && user.getSex() !=null,"sex",user.getSex());

		List<User> userList = userMapper.selectList(queryWrapper);
		System.out.println(userList);
	}

	/**
	 * 案例6:
	 * 		查询用户Id = 1,3,5,6,7
	 * Sql: select * from user where id in (1,3,5,6,7)
	 */
	@Test
	public void testSelect06(){
		//规则: 如果数组需要转化集合则使用包装类型
		Integer[] ids = {1,3,5,6,7};
		//将数组转化为集合
		List<Integer> idList = Arrays.asList(ids);
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.in("id",idList);
		List<User> userList = userMapper.selectList(queryWrapper);
		//方法2:
		List<User> userList2 = userMapper.selectBatchIds(idList);
		System.out.println(userList);
		System.out.println(userList2);
	}

	/**
	 * 删除name为null的数据
	 */
	@Test
	public void delete(){
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.isNull("name");
		userMapper.delete(queryWrapper);
		System.out.println("删除成功!!!");
	}

	/**
	 * 更新操作
	 * 案例1:  将id=74的用户的名称改为 你好SpringBoot age=18 sex="其他"
	 * 案例2:  将name="测试Mybatis" 的数据改为 age=100 sex=男
	 */
	@Test
	public void update(){
		User user = new User();
		user.setId(74).setName("你好SpringBoot").setAge(18).setSex("其他");
		//Sql: update user set name="xxx", age=18,sex='xx' where id=xx
		//Sql形成的原则 根据对象中不为null的属性当做  set条件. 并且将Id当做where条件
		userMapper.updateById(user);

		User userTemp = new User();
		userTemp.setAge(100).setSex("男");
		UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("name", "测试Mybatis");
		userMapper.update(userTemp,updateWrapper);
		System.out.println("更新成功!!!");

	}








}
