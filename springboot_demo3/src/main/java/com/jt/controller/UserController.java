package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

//@RestController //1.返回数据是JSON数据  2.表示Ajax请求的结束 3.返回值的是字符串本身
                // 4.不会执行SpringMVC中的组件 视图解析器
@Controller     //跳转到指定的页面中 会执行视图解析器 进行路径的拼接 前缀+后缀
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 请求用户请求: http://localhost:8090/findAll
     * 参数:  暂时没有
     * 返回值: 特定页面名称
     * 前缀:  /WEB-INF/
     * 页面名称: userList
     *  后缀: .jsp
     */

    //方式1: 页面同步请求的方法
    @RequestMapping("/findAll")
    public String findAll(Model model){
        List<User> userList = userService.findAll();
        List<Integer> ids = new ArrayList<>();
        ids.add(1);
        ids.add(2);
        ids.add(3);

        //目的:需要通过request对象将数据带到页面中进行展现
        //model对象对request进行了封装 方法更加丰富
        model.addAttribute("userList",userList);
        model.addAttribute("msg", "你好SpringBoot");
        model.addAttribute("ids",ids);

        //springMVC中采用视图解析器的形式 添加 前缀+后缀
        return "userList";  //返回页面逻辑名称
    }

    /**
     *方式2:
     * 异步请求: 多次请求 多次响应
     * 实现页面跳转
     * URL网址: http://localhost:8090/toAjax
     * 跳转页面名称: ajax.jsp
     */
    @RequestMapping("/toAjax")
    public String toAjax(Model model){

        //返回的应该是页面的逻辑名称
        return "ajax";
    }

    /**
     * 实现ajax业务调用
     * URL地址:  /ajaxUser
     * 参数:     id/name属性
     * 返回值:   List<User>
     */
    @RequestMapping("/ajaxUser")
    @ResponseBody
    public List<User> ajaxUser(Integer id,String name){
        System.out.println("id参数:"+id);
        System.out.println("name参数:"+name);
        return userService.findAll();
    }
}
