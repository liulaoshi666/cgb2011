<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<!-- 导入函数类库 -->
	<script src="../js/jquery-3.4.1.min.js"></script>
	
	<!-- 编辑页面JS -->
	<script>
		//让页面加载完成之后 再执行
		$(function(){
			
			/**
			 * 常见ajax种类:
			 * 		$.get("url地址","传递的参数",回调函数,返回值类型)
			 * 		$.post("url地址","传递的参数",回调函数,返回值类型)
			 * 		$.getJSON("url地址","传递的参数",回调函数)
			 * 		$.ajax({.....})
			 */
			
			/* 1.ajax常规用法 get请求
				
				参数说明: 
					1.url地址     写法1: http://host:port/xxxx请求路径 跨域请求方式(别人的服务器)
								  写法2:	/xxxx		同域请求  访问的自己的服务器
								  
					2.参数写法	  
								  写法1:	 {key1:value1,key2:value2}  {"id":1000}
								  写法2:  key1=value1&key2=value2    "id=1000&name=tomcat"
					3.回调函数
			 */
			$.get("/ajaxUser","id=1000&name=tomcat",function(result){
				//console.log(result)
				//alert("回调函数执行成功!!!!")
				//将返回值结果进行处理
				
				/* 基础for循环
				for(let i=0;i<result.length; i++){
					console.log(result[i])
				} */
				
				/* 增强for循环 in 表示将小标传递给index  */
				/* for(let index in result){
					console.log(result[index])
				} */
				
				/* of 将result集合的元素赋值给user */
				for(let user of result){
					let id = user.id
					let name = user.name
					let age = user.age
					let sex = user.sex
					let tr = "<tr align='center'><td>"+id+"</td><td>"+name+"</td><td>"+age+"</td><td>"+sex+"</td></tr>"
					//将tr元素追加到表格中
					$("#tab1").append(tr)
				}
			})
			
			
			
			//2. $.ajax相关说明 最根本的ajax业务调用规则
			//   函数(方法)式编程   
			$.ajax({
				type: "get",		//请求类型  $.getJSON
				url:  "/ajaxUser",	//请求网址
				data: {id:"2000",name:"tomcat猫"},	//传递的参数
				dataType: "json",		//定义服务器返回值类型 可以智能判断 可以省略不写
				success: function(result){
					//result是服务器返回的数据..
					console.log(result)
				},
				error:	function(result){
					
					alert("服务器异常,请稍后")
				},
				async: true
				//设定异步操作  如果涉及到ajax嵌套问题 则设置同步调用.
			})
		})
	</script>
	
<title>您好Springboot</title>
</head>
<body>
	<table  id="tab1" border="1px" width="65%" align="center">
		<tr>
			<td colspan="6" align="center"><h3>学生信息</h3></td>
		</tr>
		<tr>
			<th>编号</th>
			<th>姓名</th>
			<th>年龄</th>
			<th>性别</th>
		</tr>
	</table>
</body>
</html>