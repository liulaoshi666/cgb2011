package com.jt;

import org.junit.jupiter.api.Test;

import java.io.File;

public class RemoveTarget {

    //将文件打包为zip  ZipOutPutStream

    @Test
    public void remove(){
        File fileDir = new File("D:\\JT-SOFT\\课堂代码\\2011\\day18\\项目代码");
        fileDir(fileDir);

    }

    public void fileDir(File fileDir){
        File[] files = fileDir.listFiles();
        if(files !=null && files.length>0) {
            for (File file : files) {
                if (file.isDirectory()) {
                    if ("target".equals(file.getName())) {
                        deleteFile(file);
                    }
                    fileDir(file);
                }
            }
        }
    }

    public void deleteFile(File file){
            File[] files = file.listFiles();
            if(files !=null && files.length > 0){
                for (File f : files){
                    System.out.println("删除文件:"+f.getPath());
                    deleteFile(f);
                }
            }
        file.delete();
    }

}
